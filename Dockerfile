FROM golang:1.16.4-alpine3.13 as builder

ENV GO111MODULE=on \
  	CGO_ENABLED=1  \
  	GOARCH="amd64" \
  	GOOS=linux
LABEL maintainer="TV"

RUN apk update && apk add make git pkgconfig gcc g++ bash
# RUN git clone https://github.com/edenhill/librdkafka.git  && \
#   cd librdkafka && \
#   ./configure --prefix /usr && \
#   make && \
#   make install

WORKDIR /app
COPY . .

RUN go mod download
RUN go build -tags musl --ldflags "-extldflags -static" main.go

FROM alpine:3.11

LABEL maintainer="TV"

WORKDIR /app

COPY --from=builder /app/main ./
COPY --from=builder /app/config ./config

RUN apk update && apk add --no-cache tzdata bash jq gettext util-linux curl && \
    cp /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime && \
	rm -rf /var/lib/{apt,dpkg,cache,log}/ 

# COPY entrypoint.sh /entrypoint.sh
# RUN chmod +x /entrypoint.sh
# ENTRYPOINT [ "/entrypoint.sh" ]
RUN adduser -D thietlv
USER thietlv
CMD ["./main", "-config=config"]
