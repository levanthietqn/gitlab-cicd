package v1

import (
	"net/http"
	"struct/response"

	apiServiceV1 "struct/services/api/v1"

	"github.com/gin-gonic/gin"
)
type product struct{}

var Product product

func (product) GetAll(c *gin.Context) {
	platform := c.Param("platform")
	res, err := apiServiceV1.Product.GetAll(platform)
	if err != nil {
		c.JSON(http.StatusOK, response.FormatResultAPIWithCode(response.CODE_SOMETHING_WRONG, err.Error(), nil))
		return
	}

	c.JSON(http.StatusOK, response.FormatResultAPI(res))
}