module struct

go 1.15

require (
	github.com/getsentry/raven-go v0.2.0
	github.com/getsentry/sentry-go v0.11.0 // indirect
	github.com/gin-contrib/sentry v0.0.0-20191119142041-ff0e9556d1b7
	github.com/gin-gonic/gin v1.4.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.9.0
	gorm.io/driver/mysql v1.1.3
	gorm.io/gorm v1.22.2
	moul.io/http2curl v1.0.0
)
