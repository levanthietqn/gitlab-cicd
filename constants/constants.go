package constants

// Whitelist logger url
var WHITELIST_URL_LOG = map[string]int{
	"/v1/health-check": 1,
}

const (
	// Cache time to live -  by second
	CACHE_TTL_NO_LIMIT = -1 // strongly NOT recommended
	CACHE_TTL_MINUTE_1 = 60
	CACHE_TTL_MINUTE_5 = 5 * 60
	CACHE_TTL_HOUR_1   = 60 * 60
	CACHE_TTL_DAY_1    = 24 * 60 * 60
	TLL_LOCK_ORDER     = 15
	TTL_KVCACHE        = 60

	// Datetime format
	DATETIME_FORMAT_DEFAULT = "20060102150405"

	// Default time Epoch
	TIME_EPOCH = "1970-01-01 00:00:00"
)
