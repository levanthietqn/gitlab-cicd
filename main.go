package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"struct/config"
	"struct/routers"
	"struct/storage/infras"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func main() {
	fmt.Println("numCPU:", runtime.NumCPU())
	runtime.GOMAXPROCS(runtime.NumCPU())
	flagConfig := flag.String("config", "config", "Set config path")
	flag.Parse()

	run(*flagConfig)
	defer stop()

	// Set run mode
	runmode := config.GetConfig().Server.PROD_MODE
	if runmode {
		gin.SetMode(gin.ReleaseMode)
	}

	routers.InitPaymentApi(os.Getenv("PORT"))
	logrus.Infoln("Stop Done")
}

func run(configPath string) {
	err := config.Load(configPath)
	if err != nil {
		log.Printf("Error read config: %s", err.Error())
		os.Exit(1)
	}
	routers.InitLogger(config.GetConfig().Server.PROD_MODE)

	infras.InitMysql(config.GetConfig().Mysql)
}

func stop() {
	infras.CloseMysql()
}
