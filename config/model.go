package config

// Configurations exported
type Configurations struct {
	Server ServerConfigurations
	Mysql  MysqlConfigurations
}

// ServerConfigurations exported
type ServerConfigurations struct {
	Port      int
	PROD_MODE bool
}

// MysqlConfigurations exported
type MysqlConfigurations struct {
	URI             string
	DBAutoMigration bool
	Debug           bool
	MaxIdleConns    int
	MaxOpenConns    int
	ConnMaxLifeTime int
	TimeOut         int
}
