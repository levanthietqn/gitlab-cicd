package v1

import (
	"struct/response"
	"struct/storage/mysql/model"
	"struct/storage/mysql/schema"
)

type product struct{}

var Product product

func (product) GetAll(platform string) (res response.ListAllResp, err error) {
	var data []schema.ProductOutputStruct
	data, err = model.Product().GetAll(platform)
	res.Records = data
	res.Total = len(data)
	return
}
