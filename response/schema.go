package response

const CODE_SUCCESS = 0
const CODE_NOT_EXIST = 1
const CODE_SOMETHING_WRONG = 99

const MESSAGE_SUCCESS = "Thành công"
const MESSAGE_NOT_EXIST = "not exist"
const MESSAGE_PARAMS_INVALID = "Param(s) invalid"
