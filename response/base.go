package response

import (
	"reflect"

	"github.com/gin-gonic/gin"
)

type ListAllResp struct {
	Records interface{} `json:"records"`
	Total   int         `json:"total"`
}

func FormatResultAPI(data interface{}) interface{} {
	dataR := gin.H{
		"code":    CODE_SUCCESS,
		"message": MESSAGE_SUCCESS,
		"result":  data,
	}

	if InterfaceIsNil(data) {
		dataR["result"] = make(map[string]interface{})
		return dataR
	}

	return dataR
}

func FormatResultAPIWithCode(code int, errStr string, data interface{}) interface{} {
	dataR := gin.H{
		"code":    code,
		"message": errStr,
		"result":  data,
	}

	if InterfaceIsNil(data) {
		dataR["result"] = make(map[string]interface{})
		return dataR
	}

	return dataR
}

func InterfaceIsNil(v interface{}) bool {
	return v == nil || v == "" || (reflect.ValueOf(v).Kind() == reflect.Ptr && reflect.ValueOf(v).IsNil())
}