package routers

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	apiControllerV1 "struct/controllers/api/v1"
	"struct/middlewares"

	"github.com/getsentry/raven-go"
	"github.com/gin-contrib/sentry"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func InitPaymentApi(port string) {
	router := initRouterDefault()
	initUrls(router)
	s := &http.Server{
		Addr:         fmt.Sprintf(":%s", port),
		Handler:      router,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	fmt.Println("Listening and serving HTTP on :", port)
	s.ListenAndServe()
}

func initRouterDefault() *gin.Engine {
	router := gin.Default()
	router.Use(sentry.Recovery(raven.DefaultClient, false))
	router.Use(middlewares.RequestLog())

	f, _ := os.Create("log/error.log")
	gin.DefaultErrorWriter = io.MultiWriter(f)
	return router
}

func initUrls(router *gin.Engine) {
	v1 := router.Group("/v1")
	{
		v1.GET("health-check", checkHealth)
		v1.GET("products/:platform", apiControllerV1.Product.GetAll)
	}
}

// InitLogger ...
func InitLogger(runmode bool) {
	// More configs at https://godoc.org/github.com/sirupsen/logrus
	/* LOG level
	log.Trace("Something very low level.")
	log.Debug("Useful debugging information.")
	log.Info("Something noteworthy happened!")
	log.Warn("You should probably take a look at this.")
	log.Error("Something failed but I'm not quitting.")
	log.Fatal("Bye.") // Calls os.Exit(1) after logging
	log.Panic("I'm bailing.") // Calls panic() after logging
	*/

	if runmode {
		// log to json format for friendly with logstash/graylog/etc...
		logrus.SetFormatter(&logrus.JSONFormatter{})
		logrus.SetLevel(logrus.InfoLevel)
	} else {
		logrus.SetLevel(logrus.DebugLevel)
	}

	logrus.SetOutput(os.Stdout)
}

func checkHealth(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "pong",
	})
}
