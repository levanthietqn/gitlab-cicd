package middlewares

import (
	"struct/helpers"

	"github.com/gin-gonic/gin"
)

// RequestLog ...
func RequestLog() gin.HandlerFunc {
	return func(c *gin.Context) {
		helpers.Logger.ProcRequestLog(c)
		c.Next()
	}
}
