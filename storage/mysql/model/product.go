package model

import (
	"strings"
	"struct/storage/infras"
	"struct/storage/mysql"
	"struct/storage/mysql/schema"
)

type product struct {
	mysql.Base
}

var iProduct product

func Product() product {
	return iProduct
}

//GetAllFetch all sell product data
func (product) GetAll(platform string) (productOutput []schema.ProductOutputStruct, err error) {
	var product []schema.Product
	err = infras.GetMysql().
		Where("status=? AND started_at <= SYSDATE() AND ended_at > SYSDATE() AND product_type=?", 10, 10).
		Order("display_order").
		Find(&product).Error
	if err != nil {
		return productOutput, err
	}
	result := []schema.ProductOutputStruct{}
	platform = strings.ToLower(platform)
	for _, product := range product {
		if strings.Contains(strings.ToLower(product.SellingOnPlatform), platform) {
			var pOutPut schema.ProductOutputStruct
			pOutPut.ProductID = product.ProductID
			pOutPut.ProductName = product.ProductName
			pOutPut.ProductDescription = product.ProductDescription
			pOutPut.ProductType = product.ProductType
			pOutPut.Price = product.Price / 100
			pOutPut.Point = product.Price / 100000
			pOutPut.IosProductID = product.IosProductID
			pOutPut.AndroidProductID = product.AndroidProductID
			result = append(result, pOutPut)
		}
	}

	return result, nil
}