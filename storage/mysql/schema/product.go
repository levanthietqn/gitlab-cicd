package schema

import "gorm.io/gorm"

type Product struct {
	gorm.Model
	ProductID           string `gorm:"not_null; type:varchar(50)" json:"product_id"`
	ProductName         string `gorm:"not_null; type:varchar(100)" json:"product_name"`
	ProductDescription  string `gorm:"type:varchar(250)" json:"product_description"`
	ProductType         int    `gorm:"not_null; type:tinyint; default:10" json:"product_type"`
	Status              int    `gorm:"not_null; type:tinyint; default:0" json:"status"`
	StartedAt           string `gorm:"not_null; type:datetime" json:"started_at"`
	EndedAt             string `gorm:"not_null; type:datetime" json:"ended_at"`
	SellingOnPlatform   string `gorm:"type:varchar(250)" json:"selling_on_platform"`
	Price               int    `gorm:"not_null; type:integer" json:"price"`
	PriceOld            int    `gorm:"not_null; type:integer; default:0" json:"price_old"`
	UnitPrice           string `gorm:"not_null; type:varchar(10)" json:"unit_price"`
	IosProductID        string `gorm:"type:varchar(100)" json:"ios_product_id"`
	AndroidProductID    string `gorm:"type:varchar(100)" json:"android_product_id"`
	DisplayOrder        int    `gorm:"not_null; type:integer" json:"display_order"`
	PackageGroupID      string `gorm:"not_null; type:varchar(50)" json:"package_group_id"`
	PackageDuration     int    `gorm:"type:integer" json:"package_duration"`
	PackageDurationUnit int    `gorm:"type:integer" json:"package_duration_unit"`
	SMSSyntax           string `gorm:"type:text" json:"sms_syntax"`
}

type ProductOutputStruct struct {
	ProductID          string `json:"productId"`
	ProductName        string `json:"productName"`
	ProductDescription string `json:"productDescription"`
	ProductType        int    `json:"productType"`
	Price              int    `json:"price"`
	Point              int    `json:"point"`
	IosProductID       string `json:"iosProductId"`
	AndroidProductID   string `json:"androidProductId"`
}

type PackageOutputStruct struct {
	ProductID           string `json:"productId"`
	ProductName         string `json:"productName"`
	ProductDescription  string `json:"productDescription"`
	ProductType         int    `json:"productType"`
	Price               int    `json:"price"`
	IosProductID        string `json:"iosProductId"`
	AndroidProductID    string `json:"androidProductId"`
	PackageDuration     int    `json:"packageDuration"`
	PackageDurationUnit int    `json:"packageDurationUnit"`
	SMSSyntax           string `json:"smsSyntax"`
}

type ProductInfoStruct struct {
	ProductID           string `json:"product_id"`
	ProductName         string `json:"product_name"`
	ProductDescription  string `json:"product_description"`
	ProductType         int    `json:"product_type"`
	Price               int    `json:"price"`
	IosProductID        string `json:"ios_product_id"`
	AndroidProductID    string `json:"android_product_id"`
	PackageDuration     int    `json:"packageDuration"`
	PackageDurationUnit int    `json:"packageDurationUnit"`
}

type ProductResponse struct {
	Records []ProductOutputStruct `json:"records"`
	Total   int                   `json:"total"`
}

var TableProduct = "product"

// TableName ...
func (product *Product) TableName() string {
	return TableProduct
}

func (product *ProductOutputStruct) TableName() string {
	return TableProduct
}
