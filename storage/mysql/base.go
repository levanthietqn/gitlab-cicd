package mysql

import (
	"struct/storage/infras"

	"gorm.io/gorm"
)

const (
	defaultLimit = 1000
)

type Base struct{}

func (Base) GetByID(id string, value interface{}) (err error) {
	if len(id) == 0 {
		err = gorm.ErrRecordNotFound
		return
	}
	err = infras.GetMysql().
		Where("id = ?", id).First(value).Error
	return
}

func (Base) GetByIDs(ids []string, value interface{}) (err error) {
	if len(ids) == 0 {
		err = gorm.ErrRecordNotFound
		return
	}
	err = infras.GetMysql().
		Where("id IN (?)", ids).Find(value).Error
	return
}

func (Base) ListByOffsetLimit(values interface{}, offset, limit int) (total int64, err error) {
	if limit > defaultLimit || limit == 0 {
		limit = defaultLimit
	}
	err = infras.GetMysql().Model(values).Count(&total).Error
	if err != nil {
		return
	}
	err = infras.GetMysql().
		Order("created_at desc").
		Limit(limit).Offset(offset).
		Find(values).
		Error
	return
}
