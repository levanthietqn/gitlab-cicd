package infras

import (
	"database/sql"
	c "struct/config"
	"time"

	log "github.com/sirupsen/logrus"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type Mysql struct {
	db    *gorm.DB
	sqlDb *sql.DB
}

var instance *Mysql

func InitMysql(configs c.MysqlConfigurations) {
	config := gorm.Config{
		QueryFields: true,
	}
	if configs.Debug {
		config.Logger = logger.Default.LogMode(logger.Info)
	}
	db, err := gorm.Open(mysql.Open(configs.URI), &config)
	if err != nil {
		log.Fatal("faild to connect sql")
	}
	sqlDb, err := db.DB()
	if err != nil {
		log.Fatal("faild to connect sql")
	}
	sqlDb.SetMaxIdleConns(configs.MaxIdleConns)
	sqlDb.SetConnMaxLifetime(time.Duration(configs.ConnMaxLifeTime) * time.Millisecond)
	sqlDb.SetMaxOpenConns(configs.MaxOpenConns)
	instance = &Mysql{
		db:    db,
		sqlDb: sqlDb,
	}
}

func GetMysql() (db *gorm.DB) {
	if instance == nil {
		return
	}
	return instance.db
}

func CloseMysql() {
	instance.sqlDb.Close()
}
