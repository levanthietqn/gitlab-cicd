package helpers

import (
	"fmt"
	"math/rand"
	"runtime"
	"strconv"
	"strings"
	ecode "struct/constants"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"moul.io/http2curl"
)

var Logger logger

type logger struct{}

// LogTraceID ...
func LogTraceID() string {
	curTime := time.Now().Format(ecode.DATETIME_FORMAT_DEFAULT)
	return "TID" + ":" + curTime + ":" + fmt.Sprint(_randomNumber(1, 1e6))
}

func _randomNumber(min int, max int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(max-min) + min
}

func (logger) ProcRequestLog(c *gin.Context) {
	txnID := LogTraceID()
	c.Set("x-request-id", txnID)                 // set to request param
	c.Writer.Header().Set("X-Request-Id", txnID) // set to request header
	if _, exists := ecode.WHITELIST_URL_LOG[strings.ToLower(strings.TrimSpace(c.Request.RequestURI))]; !exists {
		curl, _ := http2curl.GetCurlCommand(c.Request)
		Logger.Log(txnID, "").Infoln(fmt.Sprint(curl))
	}
}

// Log ...
func (logger) Log(requestID, extraData string) *logrus.Entry {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		panic("Could not get context info for logger!")
	}

	filename := file[strings.LastIndex(file, "/")+1:] + ":" + strconv.Itoa(line)
	funcname := runtime.FuncForPC(pc).Name()
	fn := funcname[strings.LastIndex(funcname, ".")+1:]
	if extraData == "" {
		return logrus.WithField("file", filename).WithField("func", fn).WithField("txn", requestID)
	}
	return logrus.WithField("file", filename).WithField("func", fn).WithField("txn", requestID).WithField("ext", extraData)
}